package com.amir;

import org.apache.log4j.Logger;
public class SortingApp {
    private final static int MAXIMUM_ARGUMENT_AMOUNT = 10;
    private static Logger logger = Logger.getLogger(SortingApp.class);

    public static void main(String[] args) {
        //this method is static for command line interaction
        SortingApp sortingApp = new SortingApp();
        if (args.length > MAXIMUM_ARGUMENT_AMOUNT) {
            throw new IllegalArgumentException("More than 10 arguments are not allowed");
        }
        int[] integers = sortingApp.turnToIntegers(args);
        sortingApp.bubbleSortIntegerArray(integers);
        sortingApp.printIntegers(integers);
    }

    private void printIntegers(int[] integers) {
        for (int i = 0; i < integers.length; i++) {
            System.out.print(integers[i]);
            if (i != integers.length - 1) {
                System.out.print(" ");
            }
        }
    }

    private int[] turnToIntegers(String[] strings) {
        int[] integers = new int[strings.length];
        try {
            for (int i = 0; i < strings.length; i++) {
                integers[i] = Integer.parseInt(strings[i]);
            }
        } catch (NumberFormatException e) {
            logger.error("Incorrect command line input");
            throw new NumberFormatException("wrong number in input");
        }
        logger.info("String array successfully turned into integer array");
        return integers;
    }

    private void bubbleSortIntegerArray(int[] integers) {
        int temp = 0;
        for (int i = 0; i < integers.length; i++) {
            for (int j = 0; j < integers.length - 1 - i; j++) {
                if (integers[j] > integers[j + 1]) {
                    temp = integers[j];
                    integers[j] = integers[j + 1];
                    integers[j + 1] = temp;
                }
            }
        }
        logger.info("Integer array successfully sorted");
    }

}
