package com.amir;

import org.junit.Test;


public class SortingAppMoreThanTenTest {
    @Test(expected = IllegalArgumentException.class)
    public void moreThanTenArgumentsTest() {
        SortingApp.main(new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"});
    }

}
