package com.amir;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.*;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SortingAppSortingTest {

    private final String[] args;
    private final String expected;
    private final static String PRINT_STREAM_PATH = "src/test/resources/testPrintStream.txt";

    public SortingAppSortingTest(String[] args, String[] expected) {
        this.args = args;
        this.expected = expected[0];
    }

    @Parameterized.Parameters
    public static List<String[][]> testCases() {
        return Arrays.asList(new String[][][]{
                {{}, {null}},
                {{"2"}, {"2"}},
                {{"9", "10", "5", "6", "3", "1", "4", "2", "8", "7"}, {"1 2 3 4 5 6 7 8 9 10"}},
                {{"2", "4", "3"}, {"2 3 4"}},
                {{"-100", "250", "13"}, {"-100 13 250"}},
        });
    }

    @Test
    public void sortingTest() {
        try (
                FileReader fileReader = new FileReader(PRINT_STREAM_PATH);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
        ) {
            System.setOut(new PrintStream(new File(PRINT_STREAM_PATH)));
            SortingApp.main(args);
            String actual = bufferedReader.readLine();
            assertEquals(expected, actual);
        } catch (IOException fnfException) {
            fnfException.printStackTrace();
        }
    }

}
